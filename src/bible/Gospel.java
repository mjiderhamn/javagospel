package bible;

/**
 * @author Mattias Jiderhamn
 */
public class Gospel {
  public static void main(String[] args) {
    God god = God.getInstance();
    
    god.createUniverse();
    
    Man man = god.createMan();

    man.setGod(null);
    man.sin();
    
    ManDAO heaven = god.getEternity();
    try {
      heaven.persist(man);
    }
    catch (SinFoundException ex) {
      Man jesus = (Man) god;
      jesus.dieForSinOf(man);
    
      heaven.persist(man);
    }
  }
}